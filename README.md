# Bpmn-NG2

Egy demo bemutató ahhoz, hogy hogyan is kell illeszteni a bpmn-js-t az angular-cli rendszerbe. 

## install lépések
  1. checkout
  2. npm install
  3. bower install
  4. ng serve

A bower csak arra van, hogy a bpmn-t feltegyük.

bower install bpmn-js --save a bower.json-nek megfelelő parancs.
 
## Ami lényegileg történik:
  1. feltesszük bower-el a szükséges js-eket
  2. szólunk angular-cli-nek, hogy viselkedjen úgy, mintha index.html-ben húznánk be (lsd angular-cli.json)
  3. elkérjük a window.BpmnJS-től a referenciát
  4. példányosítunk egy új bpmn viewer-t amit a #canvas-hoz bindelünk
  5. lekérjük a szerverről az aktuális .bpmn filet
  6. megmondjuk a viewer-nek, hogy ezt a filet rajzolja ki
  
## Alternatív megközelítés
Létezik az npm-en megtalálható browserify-os verzió is. 

Ez alapjában véve annyiban különbözik, hogy CommonJS (RequireJS) modul formátumban van a kód.

Ezt is belehetne kötni, és több szempontból előnyös is lenne, mert jobb lenne a kód integráltság 
a projektbe, viszont webpack-es angular-cli-s környezetbe túl mélyen kéne ehhez belenyúlni. 
