import {Component, OnInit} from '@angular/core';
import {Http, Response} from "@angular/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Pizza Collaboration Viewer';
  BpmnViewer:any;

  private _http:Http;

  constructor (_http: Http) {
    this._http = _http;
    this.BpmnViewer = window['BpmnJS'];
  }

  ngOnInit() {
    let viewer = new this.BpmnViewer({ container: '#canvas' });
    this._http.get('/assets/pizza-collaboration.bpmn')
      .subscribe(res => {
        return viewer.importXML(res.text(), function(err) {
          if (!err) {
            console.log('success!');
            viewer.get('canvas').zoom('fit-viewport');
          } else {
            console.log('something went wrong:', err);
          }
        });
      });
  }
}
