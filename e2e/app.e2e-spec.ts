import { BpmnNg2Page } from './app.po';

describe('bpmn-ng2 App', function() {
  let page: BpmnNg2Page;

  beforeEach(() => {
    page = new BpmnNg2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
